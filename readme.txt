Java implementation of 6 stone game of Kalah using Spring MVC.

Takes advantage of springboot's embedded Tomcat so can be run as java application or from cmd using
mvn spring-boot:run

Application can then be accessed from
localhost:8080