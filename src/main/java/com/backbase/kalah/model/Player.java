package com.backbase.kalah.model;

/**
 * Object representing a person playing the game.
 * 
 * @author sianw
 */
public class Player {

	/**
	 * Name of the player.
	 */
	private String name;

	/**
	 * Number of the player.
	 */
	private int number;

	/**
	 * Position of the player's kalah on the board.
	 */
	private int kalah;

	/**
	 * Position of the player's opponent's kalah on the board.
	 */
	private int oppositeKalah;

	/**
	 * Position of the player's first pit on the board.
	 */
	private int pitStart;

	/**
	 * Initialise player with given name and player number. Set the position of
	 * the players pit start, kalah and opponents kalah depending on player
	 * number.
	 * 
	 * @param number
	 * @param name
	 */
	public Player(int number, String name) {
		this.number = number;
		this.name = name;
		if (number == 1) {
			this.kalah = 6;
			this.oppositeKalah = 13;
			this.pitStart = 0;
		} else {
			this.kalah = 13;
			this.oppositeKalah = 6;
			this.pitStart = 7;
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNumber() {
		return number;
	}

	public int getKalah() {
		return kalah;
	}

	public int getOppositeKalah() {
		return oppositeKalah;
	}

	public int getPitStart() {
		return pitStart;
	}
}