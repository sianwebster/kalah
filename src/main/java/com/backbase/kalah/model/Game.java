package com.backbase.kalah.model;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Object representing a game of Kalah including methods for actions that can be
 * performed on a game.
 * 
 * @author sianw
 */
public class Game {

	private static final Logger LOG = LoggerFactory.getLogger(Game.class);

	public static final int TOTAL_BOARD_SIZE = 14;
	private static final int TOTAL_NUMBER_OF_PITS = 12;

	/**
	 * List of integers that represent the number of stones in each pit on the
	 * board.
	 */
	private List<Integer> board;

	/**
	 * Numerical representation of the current player taking a turn. This is
	 * changed after a turn has been taken.
	 */
	private int currentPlayerNumber;

	/**
	 * List of the current game players.
	 */
	private List<Player> players;

	/**
	 * Boolean representing whether the game has ended due to a player having 0
	 * stones remaining in their pits.
	 */
	private boolean gameOver = false;

	/**
	 * A message that is returned to the user, such as when a player has stolen
	 * stones or the game has ended.
	 */
	private String message;

	/**
	 * Initialise board with 6 stones in each pit and 0 in each kalah. Set list
	 * of players and starting player to player one.
	 */
	public Game(Player playerOne, Player playerTwo) {
		players = Arrays.asList(playerOne, playerTwo);
		board = Arrays.asList(6, 6, 6, 6, 6, 6, 0, 6, 6, 6, 6, 6, 6, 0);
		currentPlayerNumber = 0;
	}

	public List<Integer> getBoard() {
		return board;
	}

	public void setBoard(List<Integer> board) {
		this.board = board;
	}

	public int getCurrentPlayerNumber() {
		return currentPlayerNumber;
	}

	public void setCurrentPlayerNumber(int currentPlayerNumber) {
		this.currentPlayerNumber = currentPlayerNumber;
	}

	public List<Player> getPlayers() {
		return players;
	}

	public void setPlayers(List<Player> players) {
		this.players = players;
	}

	public boolean isGameOver() {
		return gameOver;
	}

	public void setGameOver(boolean gameOver) {
		this.gameOver = gameOver;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * Clears the game message
	 */
	public void clearMessages() {
		this.message = null;
	}

	/**
	 * Finds the number of stones in the given pit and removes them from that
	 * pit and moves them into the given new pit.
	 * 
	 * @param pitToTakeFrom
	 * @param pitToMoveTo
	 */
	public void moveStonesBetweenPits(int pitToTakeFrom, int pitToMoveTo) {
		int numberOfStones = board.get(pitToTakeFrom);
		LOG.debug("Moving {} stones from pit {} to pit {}", numberOfStones, pitToTakeFrom, pitToMoveTo);
		board.set(pitToTakeFrom, 0);
		board.set(pitToMoveTo, board.get(pitToMoveTo) + numberOfStones);
	}

	/**
	 * Collects any stones remaining in player's pits and adds these to the
	 * player's kalah.
	 * 
	 * @param player
	 */
	public void collectRemainingStones(Player player) {
		LOG.debug("Collecting stones from {}'s pits and moving to the kalah", player.getName());
		for (int i = player.getPitStart(); i < player.getKalah(); i++) {
			moveStonesBetweenPits(i, player.getKalah());
		}
	}

	/**
	 * Moves all stones from the selected pit and sows them one by one into the
	 * pits counter-clockwise, skipping the opponent's kalah. The number of the
	 * last pit to be filled with a stone is returned.
	 * 
	 * @param selectedPit
	 * @param currentPlayer
	 * @return lastPitFilled
	 */
	public int sowStones(int selectedPit, Player currentPlayer) {
		LOG.debug("Sowing stones from pit {}", selectedPit);
		int numberOfStones = board.get(selectedPit);
		board.set(selectedPit, 0);

		int pitToUpdate = 0;
		int skippedKalah = 0;
		for (int i = 1; i <= numberOfStones; i++) {
			pitToUpdate = (i + selectedPit + skippedKalah) % TOTAL_BOARD_SIZE;
			if (pitToUpdate == currentPlayer.getOppositeKalah()) {
				skippedKalah++;
				pitToUpdate = (pitToUpdate + 1) % TOTAL_BOARD_SIZE;
			}
			int currentNumOfStones = board.get(pitToUpdate);
			board.set(pitToUpdate, currentNumOfStones + 1);
		}
		return pitToUpdate;
	}

	/**
	 * Moves stones from selected pit and opposite pit to current player's kalah
	 * if opposite pit contains stones. Returns the number of stones stolen.
	 * 
	 * @param selectedPit
	 * @param currentPlayer
	 * @return numOfStones
	 */
	public int stealStones(int selectedPit, Player currentPlayer) {
		int pitToStealFrom = TOTAL_NUMBER_OF_PITS - selectedPit;
		int numOfStones = board.get(pitToStealFrom);
		if (numOfStones != 0) {
			moveStonesBetweenPits(selectedPit, currentPlayer.getKalah());
			moveStonesBetweenPits(pitToStealFrom, currentPlayer.getKalah());
		}
		return numOfStones;
	}
}