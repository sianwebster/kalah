package com.backbase.kalah.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.backbase.kalah.model.Game;
import com.backbase.kalah.model.Player;

/**
 * Implements the logic of the game of Kalah.
 * 
 * @author sianw
 */
@Service
public class GameManager {

	private static final Logger LOG = LoggerFactory.getLogger(GameManager.class);

	static final String STOLEN_STONES_MESSAGE = "%s has stolen %d stones from pit %d";
	static final String PLAYER_WIN_MESSAGE = "%s has won the game with %d stones to %d";
	static final String TIE_MESSAGE = "There is a tie! Both players have %d stones";
	static final String INVALID_PIT_SELECTED = "The chosen pit is not available to be selected as it is either empty or belongs to your opponent. Please select another.";
	private static final int PLAYER_TWO_KALAH = 13;
	private static final int PLAYER_ONE_KALAH = 6;

	/**
	 * Initialises a new game of Kalah with two players using the given player
	 * names.
	 * 
	 * @param playerOneName
	 * @param playerTwoName
	 * @return game
	 */
	public Game initiateGame(String playerOneName, String playerTwoName) {
		Player playerOne = new Player(1, playerOneName);
		Player playerTwo = new Player(2, playerTwoName);
		return new Game(playerOne, playerTwo);
	}

	/**
	 * Clears game messages from previous turn. Executes given move and checks
	 * whether move has caused the game to end. Sets next player if current
	 * player does not have another move to take.
	 * 
	 * @param game
	 * @param selectedPit
	 * @return game
	 */
	public Game nextTurn(Game game, int selectedPit) {
		game.clearMessages();
		Player currentPlayer = game.getPlayers().get(game.getCurrentPlayerNumber());
		LOG.debug("Player {} has selected to move stones from pit {}", currentPlayer.getName(), selectedPit);
		makeMove(game, currentPlayer, selectedPit);
		checkForGameOver(game);
		return game;
	}

	/**
	 * Sums total stones in current players pit to determine whether the game
	 * should end. If total is 0, then all stones from opponents pits are moved
	 * to their kalah. The number of stones within the two kalahs are compared
	 * to find the winner and this information is set as a game message.
	 * 
	 * @param game
	 */
	private void checkForGameOver(Game game) {
		LOG.debug("Checking whether either player has 0 stones in their pits");
		Player playerOne = game.getPlayers().get(0);
		Player playerTwo = game.getPlayers().get(1);
		int playerOneStones = sumOfStones(game.getBoard(), playerOne.getPitStart(), playerOne.getKalah());
		int playerTwoStones = sumOfStones(game.getBoard(), playerTwo.getPitStart(), playerTwo.getKalah());
		if (playerOneStones == 0 || playerTwoStones == 0) {
			LOG.debug("Found a player with 0 stones in their pits, ending the game");
			if (playerOneStones == 0) {
				game.collectRemainingStones(playerTwo);
			} else {
				game.collectRemainingStones(playerOne);
			}
			generateEndGameMessage(game);
			game.setGameOver(true);
		}
	}

	/**
	 * Compares total of stones in the player's kalahs and generates a game
	 * message announcing the winner.
	 */
	private void generateEndGameMessage(Game game) {
		int playerOneStones = game.getBoard().get(PLAYER_ONE_KALAH);
		int playerTwoStones = game.getBoard().get(PLAYER_TWO_KALAH);
		if (playerOneStones > playerTwoStones) {
			LOG.debug("Player 1 has won the game");
			game.setMessage(String.format(PLAYER_WIN_MESSAGE, game.getPlayers().get(0).getName(), playerOneStones,
					playerTwoStones));
		} else if (playerTwoStones > playerOneStones) {
			LOG.debug("Player 2 has won the game");
			game.setMessage(String.format(PLAYER_WIN_MESSAGE, game.getPlayers().get(1).getName(), playerTwoStones,
					playerOneStones));
		} else {
			LOG.debug("The game ended in a draw");
			game.setMessage(String.format(TIE_MESSAGE, playerOneStones));
		}
	}

	/**
	 * Asks player to select a pit, checks that the selection is appropriate and
	 * if so moves the stones. If not, the player is asked to selected another
	 * pit. The last pit filled with a stone is used to determine whether the
	 * current player gets another move or can steal stones from their opponent.
	 * 
	 * @param game
	 * @param currentPlayer
	 * @param selectedPit
	 */
	private void makeMove(Game game, Player currentPlayer, int selectedPit) {
		boolean hasAnotherMove = false;
		if (currentPlayer.getPitStart() > selectedPit || selectedPit > currentPlayer.getKalah()
				|| game.getBoard().get(selectedPit) == 0) {
			LOG.debug("Invalid pit selected");
			game.setMessage(INVALID_PIT_SELECTED);
			hasAnotherMove = true;
		} else {
			int lastPitFilled = game.sowStones(selectedPit, currentPlayer);
			if (lastPitFilled == PLAYER_ONE_KALAH || lastPitFilled == PLAYER_TWO_KALAH) {
				LOG.debug("Last stone was sowed into the kalah, player has another turn");
				hasAnotherMove = true;
			} else if (game.getBoard().get(lastPitFilled) == 1 && currentPlayer.getPitStart() <= lastPitFilled
					&& lastPitFilled < currentPlayer.getKalah()) {
				LOG.debug("Last stone was sowed into empty pit, player steals stones");
				int numOfStones = game.stealStones(lastPitFilled, currentPlayer);
				if (numOfStones != 0) {
					game.setMessage(String.format(STOLEN_STONES_MESSAGE, currentPlayer.getName(), numOfStones,
							game.getCurrentPlayerNumber() == 0 ? 6 - lastPitFilled : 13 - lastPitFilled));
				}
			}
		}
		game.setCurrentPlayerNumber(
				hasAnotherMove ? game.getCurrentPlayerNumber() : (game.getCurrentPlayerNumber() + 1) % 2);
	}

	/**
	 * Sums number of stones in pits between given start pit (included) and end
	 * pit (excluded).
	 * 
	 * @param board
	 * @param startPit
	 * @param endPit
	 * @return sumOfStones
	 */
	public int sumOfStones(List<Integer> board, int startPit, int endPit) {
		int sumOfStones = 0;
		for (int i = startPit; i < endPit; i++) {
			sumOfStones += board.get(i);
		}
		return sumOfStones;
	}
}
