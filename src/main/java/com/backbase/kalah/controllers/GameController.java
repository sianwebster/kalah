package com.backbase.kalah.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.backbase.kalah.model.Game;
import com.backbase.kalah.service.GameManager;

/**
 * Controller for the game board view.
 * 
 * @author sianw
 */
@Controller
@SessionAttributes({ "game" })
public class GameController {

	/**
	 * Service layer that manipulates game
	 */
	@Autowired
	GameManager manager;

	/**
	 * Takes two optional player names uses these to intialise a new game of
	 * Kalah. Game and current player attributes are added to the model and the
	 * game board view is returned.
	 * 
	 * @param playerOneName
	 * @param playerTwoName
	 * @param model
	 * @return game board view
	 */
	@RequestMapping(value = "/startGame", method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE, produces = MediaType.TEXT_HTML_VALUE)
	public String startGame(@RequestParam(required = false, defaultValue = "Player 1") String playerOneName,
			@RequestParam(required = false, defaultValue = "Player 2") String playerTwoName, Model model) {
		Game game = manager.initiateGame(playerOneName, playerTwoName);
		model.addAttribute("game", game);
		model.addAttribute("player", game.getPlayers().get(game.getCurrentPlayerNumber()));
		return "gameBoard";
	}

	/**
	 * Takes a selected pit and executes the current player's move. The next
	 * player attribute is added to the model and the game board view is
	 * returned.
	 * 
	 * @param pit
	 * @param game
	 * @param model
	 * @return game board view
	 */
	@RequestMapping(value = "/selectPit", method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE, produces = MediaType.TEXT_HTML_VALUE)
	public String selectPit(@RequestParam(value = "pit") int pit, @ModelAttribute Game game, Model model) {
		game = manager.nextTurn(game, pit);
		model.addAttribute("player", game.getPlayers().get(game.getCurrentPlayerNumber()));
		return "gameBoard";
	}
}