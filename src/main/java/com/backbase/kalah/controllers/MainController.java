package com.backbase.kalah.controllers;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Controller for the index view.
 * 
 * @author sianw
 */
@Controller
public class MainController {

	/**
	 * Maps the index page
	 * 
	 * @return index view
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET, produces = MediaType.TEXT_HTML_VALUE)
	public String index() {
		return "index";
	}
}
