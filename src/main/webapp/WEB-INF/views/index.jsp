<!DOCTYPE html>

<html lang="en">
<head>
<meta charset="UTF-8">
<title>Kalah</title>
<link rel="stylesheet" href="/resources/css/bootstrap.min.css">
<link rel="stylesheet" href="/resources/css/kalah.css">
</head>
<body>

	<div class="container text-center">
		<div class="row">
			<h1>Kalah</h1>
		</div>
		<div class="row body">
			<div class="col-md-offset-3 col-md-6">
				<form action="/startGame" method="post">
					<div class="form-group">
						<label for="playerOneName">Player 1, please enter your name:</label>
						<input type="text" class="form-control" name="playerOneName" id="playerOneName">
					</div>
					<div class="form-group">
						<label for="playerTwoName">Player 2, please enter your name:</label>
						<input type="text" class="form-control" name="playerTwoName" id="playerTwoName">
					</div>
					<input type="submit" class="btn" value="Submit">
				</form>
			</div>
		</div>
	</div>
</body>
</html>