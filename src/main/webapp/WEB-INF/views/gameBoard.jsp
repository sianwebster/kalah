<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html lang="en">
<head>
<meta charset="UTF-8">
<title>Kalah</title>
<link rel="stylesheet" href="/resources/css/bootstrap.min.css">
<link rel="stylesheet" href="/resources/css/kalah.css">
<script async>
	function disableOpponentsPits() {
		for (var i = ${player.pitStart}; i < ${player.kalah}; i++) {
			if (${game.board}[i]!=0) {
				document.getElementById(i).disabled = false;
			}
		}
	};
</script>
</head>
<body onLoad="disableOpponentsPits()">
	<div class="container text-center">
		<div class="row">
			<h1>Kalah</h1>
		</div>
		<div class="row text-left">
			<div class="col-md-offset-2 text">
				<c:if test="${!game.gameOver}">
	    			${player.name}, it's your turn. Please select a pit:
	    		</c:if>
			</div>
		</div>
		<div class="row">
			<div class="col-md-1 col-md-offset-2 board">
				<button name="pit" class="btn kalah" value="13" id="13" disabled>
					Kalah<br>${game.board[13]} Stones
				</button>
			</div>
			<div class="col-md-6 board" align="center">
				<div class="row pits">
					<form action="/selectPit" method="post">
						<button name="pit" class="btn pit" value="12" id="12" disabled>
							Pit Six<br>${game.board[12]} Stones
						</button>
						<button name="pit" class="btn pit" value="11" id="11" disabled>
							Pit Five<br>${game.board[11]} Stones
						</button>
						<button name="pit" class="btn pit" value="10" id="10" disabled>
							Pit Four<br>${game.board[10]} Stones
						</button>
						<button name="pit" class="btn pit" value="9" id="9" disabled>
							Pit Three<br>${game.board[9]} Stones
						</button>
						<button name="pit" class="btn pit" value="8" id="8" disabled>
							Pit Two<br>${game.board[8]} Stones
						</button>
						<button name="pit" class="btn pit" value="7" id="7" disabled>
							Pit One<br>${game.board[7]} Stones
						</button>
					</form>
				</div>
				<div class="row pits">
					<form action="/selectPit" method="post">
						<button name="pit" class="btn pit" value="0" id="0" disabled>
							Pit One<br>${game.board[0]} Stones
						</button>
						<button name="pit" class="btn pit" value="1" id="1" disabled>
							Pit Two<br>${game.board[1]} Stones
						</button>
						<button name="pit" class="btn pit" value="2" id="2" disabled>
							Pit Three<br>${game.board[2]} Stones
						</button>
						<button name="pit" class="btn pit" value="3" id="3" disabled>
							Pit Four<br>${game.board[3]} Stones
						</button>
						<button name="pit" class="btn pit" value="4" id="4" disabled>
							Pit Five<br>${game.board[4]} Stones
						</button>
						<button name="pit" class="btn pit" value="5" id="5" disabled>
							Pit Six<br>${game.board[5]} Stones
						</button>
					</form>
				</div>
			</div>
			<div class="col-md-1 board">
				<button name="pit" class="btn kalah" value="6" id="6" disabled>
					Kalah<br>${game.board[6]} Stones
				</button>
			</div>
		</div>
		<div class="row text-left">
			<div class="col-md-offset-2 text">${game.message}</div>
		</div>
		<div class="row text-left">
			<h3>Rules</h3>
			<p>The game provides a Kalah board and a number of stones. The
				board has 12 small pits on each side; and a big pit, called a Kalah,
				at each end. The object of the game is to capture more stones than
				one's opponent.
			<ol>
				<li>At the beginning of the game, six stones are placed in each
					pit.</li>
				<li>Each player controls the six pits and their stones on the
					player's side of the board. The player's score is the number of
					stones in the Kalah to their right.</li>
				<li>Players take turns sowing their stones. On a turn, the
					player removes all stones from one of the pits under their control.
					Moving counter-clockwise, the player drops one stone in each pit in
					turn, including the player's own Kalah but not their opponent's.</li>
				<li>If the last sown stone lands in an empty pit owned by the
					player, and the opposite pit contains stones, both the last stone
					and the opposite stone are captured and placed into the player's
					Kalah.</li>
				<li>If the last sown stone lands in the player's Kalah, the
					player gets an additional move. There is no limit on the number of
					moves a player can make in their turn.</li>
				<li>When one player no longer has any stones in any of their
					pits, the game ends. The other player moves all remaining stones to
					their Kalah, and the player with the most stones in their Kalah
					wins.</li>
			</ol>
			It is possible for the game to end in a draw.
			</p>
		</div>
	</div>
</body>
</html>