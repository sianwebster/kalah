package com.backbase.kalah.service;

import static com.backbase.kalah.model.Game.TOTAL_BOARD_SIZE;
import static com.backbase.kalah.service.GameManager.PLAYER_WIN_MESSAGE;
import static com.backbase.kalah.service.GameManager.STOLEN_STONES_MESSAGE;
import static com.backbase.kalah.service.GameManager.TIE_MESSAGE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.backbase.kalah.model.Game;

/**
 * Test logic for game of Kalah
 * 
 * @author sianw
 */
@SpringBootTest
public class GameManagerTest {

	private static final int PLAYER_TWO = 1;
	private static final int PLAYER_ONE = 0;
	private GameManager manager = new GameManager();
	private Game game;

	/**
	 * Initialise new game before every test with stones in starting positions
	 */
	@Before
	public void initiateGame() {
		game = manager.initiateGame("Player One", "Player Two");
	}

	/**
	 * Selects a pit which will not result in a second move for the player and
	 * asserts that the board is as expected after the move is executed. Also
	 * asserts that the opponent is selected for the next turn and there are no
	 * game messages.
	 */
	@Test
	public void testMoveStones() {
		game = manager.nextTurn(game, 2);
		List<Integer> expectedBoard = Arrays.asList(6, 6, 0, 7, 7, 7, 1, 7, 7, 6, 6, 6, 6, 0);
		compareExpectedAndActualBoard(expectedBoard, game.getBoard());
		assertEquals(PLAYER_TWO, game.getCurrentPlayerNumber());
		assertNull(game.getMessage());
	}

	/**
	 * Selects a pit which will result in a second move for the player and
	 * asserts that the board is as expected after the move is executed. Also
	 * asserts that the current player is selected for the next turn and there
	 * are no game messages.
	 */
	@Test
	public void testMoveStones_AnotherTurn() {
		game = manager.nextTurn(game, 0);
		List<Integer> expectedBoard = Arrays.asList(0, 7, 7, 7, 7, 7, 1, 6, 6, 6, 6, 6, 6, 0);
		compareExpectedAndActualBoard(expectedBoard, game.getBoard());
		assertEquals(PLAYER_ONE, game.getCurrentPlayerNumber());
		assertNull(game.getMessage());
	}

	/**
	 * Selects a pit which will result in player one ending the game and asserts
	 * that the opponents stones are collected from the pits and moved to the
	 * kalah after the move is executed. Also asserts that the correct game
	 * message is returned.
	 */
	@Test
	public void testGameOver_PlayerOne() {
		game.setBoard(Arrays.asList(0, 0, 0, 0, 0, 1, 35, 6, 6, 6, 6, 6, 6, 0));
		game = manager.nextTurn(game, 5);
		List<Integer> expectedBoard = Arrays.asList(0, 0, 0, 0, 0, 0, 36, 0, 0, 0, 0, 0, 0, 36);
		compareExpectedAndActualBoard(expectedBoard, game.getBoard());
		assertTrue(game.isGameOver());
		assertEquals(String.format(TIE_MESSAGE, 36), game.getMessage());
	}

	/**
	 * Selects a pit which will result in player two ending the game and asserts
	 * that the opponents stones are collected from the pits and moved to the
	 * kalah after the move is executed. Also asserts that the correct game
	 * message is returned.
	 */
	@Test
	public void testGameOver_PlayerTwo() {
		game.setBoard(Arrays.asList(6, 6, 6, 6, 6, 6, 0, 0, 0, 0, 0, 0, 1, 35));
		game.setCurrentPlayerNumber(1);
		game = manager.nextTurn(game, 12);
		List<Integer> expectedBoard = Arrays.asList(0, 0, 0, 0, 0, 0, 36, 0, 0, 0, 0, 0, 0, 36);
		compareExpectedAndActualBoard(expectedBoard, game.getBoard());
		assertTrue(game.isGameOver());
		assertEquals(String.format(TIE_MESSAGE, 36), game.getMessage());
	}

	/**
	 * Selects a pit which will result in player one winning the game and
	 * asserts that the board is as expected after the move is executed. Also
	 * asserts that the correct game message is returned.
	 */
	@Test
	public void testPlayerOneWin() {
		game.setBoard(Arrays.asList(0, 0, 0, 0, 0, 1, 41, 0, 6, 6, 6, 6, 6, 0));
		game = manager.nextTurn(game, 5);
		List<Integer> expectedBoard = Arrays.asList(0, 0, 0, 0, 0, 0, 42, 0, 0, 0, 0, 0, 0, 30);
		compareExpectedAndActualBoard(expectedBoard, game.getBoard());
		assertTrue(game.isGameOver());
		assertEquals(String.format(PLAYER_WIN_MESSAGE, game.getPlayers().get(0).getName(), 42, 30), game.getMessage());
	}

	/**
	 * Selects a pit which will result in player two winning the game and
	 * asserts that the board is as expected after the move is executed. Also
	 * asserts that the correct game message is returned.
	 */
	@Test
	public void testPlayerTwoWin() {
		game.setBoard(Arrays.asList(0, 0, 0, 0, 0, 1, 29, 6, 6, 6, 6, 6, 6, 6));
		game = manager.nextTurn(game, 5);
		List<Integer> expectedBoard = Arrays.asList(0, 0, 0, 0, 0, 0, 30, 0, 0, 0, 0, 0, 0, 42);
		compareExpectedAndActualBoard(expectedBoard, game.getBoard());
		assertTrue(game.isGameOver());
		assertEquals(String.format(PLAYER_WIN_MESSAGE, game.getPlayers().get(1).getName(), 42, 30), game.getMessage());
	}

	/**
	 * Selects a pit which will result in the game ending in a draw and asserts
	 * that the board is as expected after the move is executed. Also asserts
	 * that the correct game message is returned.
	 */
	@Test
	public void testDraw() {
		game.setBoard(Arrays.asList(0, 0, 0, 0, 0, 1, 35, 6, 6, 6, 6, 6, 6, 0));
		game = manager.nextTurn(game, 5);
		List<Integer> expectedBoard = Arrays.asList(0, 0, 0, 0, 0, 0, 36, 0, 0, 0, 0, 0, 0, 36);
		compareExpectedAndActualBoard(expectedBoard, game.getBoard());
		assertTrue(game.isGameOver());
		assertEquals(String.format(TIE_MESSAGE, 36), game.getMessage());
	}

	/**
	 * Selects a pit which will result in stones being stolen from the
	 * opponent's pit and asserts that the board is as expected after the move
	 * is executed. Also asserts that the correct game message is returned.
	 */
	@Test
	public void testStealStones() {
		game.setBoard(Arrays.asList(1, 0, 6, 6, 6, 6, 0, 6, 6, 6, 6, 6, 6, 0));
		game = manager.nextTurn(game, 0);
		List<Integer> expectedBoard = Arrays.asList(0, 0, 6, 6, 6, 6, 7, 6, 6, 6, 6, 0, 6, 0);
		compareExpectedAndActualBoard(expectedBoard, game.getBoard());
		assertEquals(String.format(STOLEN_STONES_MESSAGE, game.getPlayers().get(0).getName(), 6, 5), game.getMessage());
	}

	/**
	 * Selects a pit which will result in stones being attempted to be stolen
	 * from the opponent's pit but failing as the pit is empty and asserts that
	 * the board is as expected after the move is executed. Also asserts that no
	 * game message is returned.
	 */
	@Test
	public void testStealStones_NoStonesInPit() {
		game.setBoard(Arrays.asList(1, 0, 6, 6, 6, 6, 0, 6, 6, 6, 6, 0, 12, 0));
		game = manager.nextTurn(game, 0);
		List<Integer> expectedBoard = Arrays.asList(0, 1, 6, 6, 6, 6, 0, 6, 6, 6, 6, 0, 12, 0);
		compareExpectedAndActualBoard(expectedBoard, game.getBoard());
		assertNull(game.getMessage());
	}

	/**
	 * Compares each element of the board with the given expected board.
	 * 
	 * @param expectedBoard
	 * @param actualBoard
	 */
	private void compareExpectedAndActualBoard(List<Integer> expectedBoard, List<Integer> actualBoard) {
		for (int i = 0; i < TOTAL_BOARD_SIZE; i++) {
			assertEquals(expectedBoard.get(i), actualBoard.get(i));
		}
	}
}