package com.backbase.kalah.model;

import static com.backbase.kalah.model.Game.TOTAL_BOARD_SIZE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

/**
 * Tests actions that can be performed on a game.
 * 
 * @author sianw
 */
public class GameTest {

	private static final int PLAYER_TWO = 1;
	private static final int PLAYER_ONE = 0;
	private Game game;

	/**
	 * Initialise game before every test with stones in starting positions.
	 */
	@Before
	public void initialiseGame() {
		game = new Game(new Player(1, "Player One"), new Player(2, "Player Two"));
	}

	/**
	 * Sets a game message and calls the clearMessages method. Asserts that this
	 * causes the message to be set back to null.
	 */
	@Test
	public void testClearMessages() {
		game.setMessage("Test Message");
		game.clearMessages();
		assertNull(game.getMessage());
	}

	/**
	 * Calls the collectRemainingStones method and asserts that all stones are
	 * collected from the pits of the opponent player and placed into their
	 * Kalah.
	 */
	@Test
	public void testCollectRemainingStones() {
		List<Integer> expectedBoard = Arrays.asList(0, 0, 0, 0, 0, 0, 36, 6, 6, 6, 6, 6, 6, 0);
		game.collectRemainingStones(game.getPlayers().get(PLAYER_ONE));
		compareExpectedAndActualBoard(expectedBoard, game.getBoard());
	}

	/**
	 * Calls the moveStonesBetweenPits method and asserts that all the stones
	 * from the given pit are removed and placed into the new pit.
	 */
	@Test
	public void testMoveStonesBetweenPits() {
		List<Integer> expectedBoard = Arrays.asList(0, 12, 6, 6, 6, 6, 0, 6, 6, 6, 6, 6, 6, 0);
		game.moveStonesBetweenPits(0, 1);
		compareExpectedAndActualBoard(expectedBoard, game.getBoard());
	}

	/**
	 * Calls the sowStones method and asserts that all the stones from the given
	 * pit are removed and placed one by one into pits counter-clockwise.
	 */
	@Test
	public void testSowStones() {
		List<Integer> expectedBoard = Arrays.asList(0, 7, 7, 7, 7, 7, 1, 6, 6, 6, 6, 6, 6, 0);
		game.sowStones(0, game.getPlayers().get(0));
		compareExpectedAndActualBoard(expectedBoard, game.getBoard());
	}

	/**
	 * Calls the sowStones method with a selected pit which will result in
	 * stones being sowed from a pit belonging to player one through all of
	 * player two's pits and asserts that the opponent's kalah did not receive a
	 * stone.
	 */
	@Test
	public void testSowStonesAvoidOpponentKalah() {
		game.setBoard(Arrays.asList(6, 6, 6, 6, 2, 10, 0, 6, 6, 6, 6, 6, 6, 0));
		List<Integer> expectedBoard = Arrays.asList(7, 7, 7, 6, 2, 0, 1, 7, 7, 7, 7, 7, 7, 0);
		game.sowStones(5, game.getPlayers().get(0));
		compareExpectedAndActualBoard(expectedBoard, game.getBoard());
	}

	/**
	 * Calls the sowStones method with a selected pit which will result in
	 * stones being sowed from a pit belonging to player two into pits belonging
	 * to player one and asserts that the board is as expected after the move is
	 * executed.
	 */
	@Test
	public void testSowStonesCircular() {
		List<Integer> expectedBoard = Arrays.asList(7, 7, 7, 7, 6, 6, 0, 6, 6, 6, 6, 0, 7, 1);
		game.sowStones(11, game.getPlayers().get(PLAYER_TWO));
		compareExpectedAndActualBoard(expectedBoard, game.getBoard());
	}

	/**
	 * Calls the stealStones method and asserts that the stones from the
	 * selected pit and the opposite pit are moved to the players kalah.
	 */
	@Test
	public void testStealStones() {
		game.setBoard(Arrays.asList(0, 1, 6, 6, 6, 6, 0, 6, 6, 6, 6, 6, 6, 0));
		game.stealStones(1, game.getPlayers().get(PLAYER_ONE));
		List<Integer> expectedBoard = Arrays.asList(0, 0, 6, 6, 6, 6, 7, 6, 6, 6, 6, 0, 6, 0);
		compareExpectedAndActualBoard(expectedBoard, game.getBoard());
	}

	/**
	 * Compares each element of the board with the given expected board.
	 * 
	 * @param expectedBoard
	 * @param actualBoard
	 */
	private void compareExpectedAndActualBoard(List<Integer> expectedBoard, List<Integer> actualBoard) {
		for (int i = 0; i < TOTAL_BOARD_SIZE; i++) {
			assertEquals("Pit " + i + " does not match expected", expectedBoard.get(i), actualBoard.get(i));
		}
	}
}
