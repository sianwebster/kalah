package com.backbase.kalah.controllers;

import java.util.Arrays;
import java.util.List;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.backbase.kalah.model.Game;
import com.backbase.kalah.model.Player;

/**
 * Tests covering the game board view controller
 * 
 * @author sianw
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class GameControllerTest {

	private static final String GAME_ATTRIBUTE = "game";
	private static final String PLAYER_ATTRIBUTE = "player";
	private static final String BOARD_ATTRIBUTE = "board";
	private static final String GAMEBOARD_VIEW = "gameBoard";
	private static final String DEFAULT_PLAYER_ONE_NAME = "Player 1";
	private static final String PLAYER_TWO_NAME = "Player Two";
	private static final String PLAYER_ONE_NAME = "Player One";
	private static final List<Integer> DEFAULT_BOARD = Arrays.asList(6, 6, 6, 6, 6, 6, 0, 6, 6, 6, 6, 6, 6, 0);

	/**
	 * Mocks all MVC infrastructure to allow for test requests to be performed
	 */
	@Autowired
	private MockMvc mvc;

	/**
	 * Mocks a request to start a game with specific player names and asserts
	 * that the request is successful and the correct view is returned with the
	 * given player name set.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testStartGame() throws Exception {
		mvc.perform(MockMvcRequestBuilders.post("/startGame").param("playerOneName", PLAYER_ONE_NAME)
				.param("playerTwoName", PLAYER_TWO_NAME).contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.view().name(GAMEBOARD_VIEW))
				.andExpect(MockMvcResultMatchers.model().attribute(GAME_ATTRIBUTE,
						Matchers.hasProperty(BOARD_ATTRIBUTE, Matchers.equalTo(DEFAULT_BOARD))))
				.andExpect(MockMvcResultMatchers.model().attribute(PLAYER_ATTRIBUTE,
						Matchers.hasProperty("name", Matchers.equalTo(PLAYER_ONE_NAME))));
	}

	/**
	 * Mocks a request to start a game with the default player names and asserts
	 * that the request is successful and the correct view is returned with the
	 * default player name set.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testStartGameDefaultPlayerName() throws Exception {
		mvc.perform(MockMvcRequestBuilders.post("/startGame").contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.view().name(GAMEBOARD_VIEW))
				.andExpect(MockMvcResultMatchers.model().attribute(GAME_ATTRIBUTE,
						Matchers.hasProperty(BOARD_ATTRIBUTE, Matchers.equalTo(DEFAULT_BOARD))))
				.andExpect(MockMvcResultMatchers.model().attribute(PLAYER_ATTRIBUTE,
						Matchers.hasProperty("name", Matchers.equalTo(DEFAULT_PLAYER_ONE_NAME))));
	}

	/**
	 * Mocks a request to select a pit to move stones from and asserts that the
	 * request is successful and the correct view is returned with the
	 * appropriately updated board.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testSelectPit() throws Exception {
		mvc.perform(MockMvcRequestBuilders.post("/selectPit").param("pit", "0")
				.contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
				.sessionAttr(GAME_ATTRIBUTE, new Game(new Player(1, PLAYER_ONE_NAME), new Player(2, PLAYER_TWO_NAME))))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.view().name(GAMEBOARD_VIEW))
				.andExpect(MockMvcResultMatchers.model().attribute(GAME_ATTRIBUTE, Matchers.hasProperty(BOARD_ATTRIBUTE,
						Matchers.equalTo(Arrays.asList(0, 7, 7, 7, 7, 7, 1, 6, 6, 6, 6, 6, 6, 0)))));
	}
}